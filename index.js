// Node.js Routing with HTTP method

// CRUD Operations					HTTP Method
	// C - reate 					POST
	// R - ead 						GET
	// U - pdate 					PUT / PATCH
	// D - elete					DELETE

const http = require('http');

// mock data
let users = [
	{
		username: 'peterIsHomeless',
		email: 'peterParker@mail.com',
		password: 'peterNoWayHome'
	},
	{
		username: 'Tony3000',
		email: 'starksIndustries@mail.com',
		password: 'ironManWillBeBack'
	}
];

let courses = [
	{
		name: 'Math 103',
		price: 2500,
		isActive: true
	},
	{
		name: 'Biology 201',
		price: 2500,
		isActive: true
	}
];

	http.createServer((req, res) => {
		if(req.url === '/' && req.method === 'GET'){
			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking GET method')
		} else if(req.url === '/' && req.method === 'POST'){
			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking POST method')
		} else if(req.url === '/' && req.method === 'PUT'){
			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking PUT method')
		} else if(req.url === '/' && req.method === 'DELETE'){
			res.writeHead(200, {'Content-Type' : 'text/plain'})
			res.end('This route is for checking DELETE method')

		// actual application with mock data
		} else if(req.url === '/users' && req.method === 'GET'){
			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(users));
/*
	Mini-Activity

	>> Create a new route with GET method
	>> This route is on "/courses" endpoint
		>> status code: 200
		>> content type for json
		>> end with showing the courses array

	>> Send your output in Hangouts
*/
		} else if(req.url === '/courses' && req.method === 'GET'){
			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(JSON.stringify(courses));

		} else if(req.url === '/users' && req.method === 'POST'){
			let requestBody = '';
			// 'on' = (on the event) method, 'data' = event, (data) = anonymous function
			req.on('data', (data) => {
				console.log(data)
				requestBody += data
			})
			req.on('end', () => {
				console.log(requestBody)
				requestBody = JSON.parse(requestBody)
				let newUser = {
					username: requestBody.username,
					email: requestBody.email,
					password: requestBody.password
				}
				users.push(newUser);
				console.log(users);

				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(JSON.stringify(users));
			})
		} else if(req.url === '/courses' && req.method === 'POST'){
			let requestBody = '';
			// data-step
			req.on('data', (data) => {
				requestBody += data
			})
			// end-step
			req.on('end', () => {
				requestBody = JSON.parse(requestBody)
				let newCourse = {
					name: requestBody.name,
					price: requestBody.price,
					isActive: requestBody.isActive
				}
				courses.push(newCourse);

				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(JSON.stringify(courses));
			})
		}
	}).listen(4000)
	console.log('Server is running at port 4000');
